// Iteración #1: Buscar el máximo
//Completa la función que tomando dos números como argumento devuelva el más alto.

function sum(numberOne, numberTwo) {
  if (numberOne < numberTwo) console.log(numberTwo, 'es mas alto')
  else if (numberOne > numberTwo) console.log(numberOne, 'es mas alto.');
  else if (numberOne = numberTwo) console.log(numberOne, 'son iguales');
}


//Iteración #2: Buscar la palabra más larga
/*Completa la función que tomando un array de strings como argumento devuelva el más largo,
en caso de que dos strings tenga la misma longitud deberá devolver el primero.
Puedes usar este array para probar tu función:*/

const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(avengers) {
  let palabra = avengers[0];
  for (let i = 1; i < avengers.length; i++) {
    if (avengers[i].length > palabra.length) {
      palabra = avengers[i];
    }
  }

  return console.log(palabra);

}
findLongestWord(avengers);





// Iteración #3: Calcular la suma
/* Calcular una suma puede ser tan simple como iterar sobre un array y sumar cada uno de los elementos.
Implemente la función denominada sumNumbers que toma un array de números como argumento y devuelve la 
suma de todos los números de la matriz. */


const numbers1 = [1, 2, 3, 5, 45, 37, 58];
let total = 0

function sumall(arr) {

  for (let i of arr) total += i;

  return console.log(total)
};

sumall(numbers1);



//Iteración #4: Calcular el promedio
/*Calcular un promedio es una tarea extremadamente común.
 Puedes usar este array para probar tu función: */

const numbers2 = [12, 21, 38, 5, 45, 37, 6];

function average(promediodearr) {
  let totalprom = 0
  for (let prom of promediodearr) totalprom += prom;

  promedio = totalprom / promediodearr.length

  return console.log(promedio)

};

average(numbers2);


//Iteración #5: Calcular promedio de strings
/* Crea una función que reciba por parámetro un array 
y cuando es un valor number lo sume y de lo contrario 
cuente la longitud del string y lo sume. Puedes usar 
este array para probar tu función:     */

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];

function averageWord(promediodecadenas) {
  totalpromDeElementos = 0
  for (promstrng of promediodecadenas)
    if (isNaN(promstrng)) {
      totalpromDeElementos += promstrng.length;
    } else {
      totalpromDeElementos += promstrng;
    }
  return console.log(totalpromDeElementos)
}
averageWord(mixedElements)


/* // Iteración #6: Valores únicos
Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados,
en caso que existan los elimina para retornar un array sin los elementos duplicados.
  Puedes usar este array para probar tu función: */


const duplicates = [
  'sushi',
  'pizza',
  'burger',
  'potatoe',
  'pasta',
  'ice-cream',
  'pizza',
  'chicken',
  'onion rings',
  'pasta',
  'soda'
];
let sinrepetir = [];

function Func(recorrerarray) {
  recorrerarray.forEach((valor) => {
    if (!sinrepetir.includes(valor)) {
      sinrepetir.push(valor);
    }
  });
  console.log(duplicates)
  console.log(`sin repetidos ${sinrepetir}`);
}
Func(duplicates);



/*         
Iteración #7: Buscador de nombres
Crea una función que reciba por parámetro un array y el valor que desea comprobar que existe 
dentro de dicho array - comprueba si existe el elemento, en caso que existan nos devuelve un 
true y la posición de dicho elemento y por la contra un false. Puedes usar este array para 
probar tu función:
 */


const nameFinder = [
  'Peter',
  'Steve',
  'Tony',
  'Natasha',
  'Clint',
  'Logan',
  'Xabier',
  'Bruce',
  'Peggy',
  'Jessica',
  'Marc'
];
console.log(nameFinder)
function Findername(valor) {
  nameFinder.forEach((element, index) => {
    if (element.includes(valor)) {
      console.log(` existe es ${true} y el index es ${index}`)

    }

  });
}

Findername('Peter'); //probar que si existe


/* **Iteration #8: Contador de repeticiones**

Crea una función que nos devuelva el número de veces que se repite cada una
 de las palabras que lo conforma.  Puedes usar este array para probar tu función: */

const counterWords = [
  'code',
  'repeat',
  'eat',
  'sleep',
  'code',
  'enjoy',
  'sleep',
  'code',
  'enjoy',
  'upgrade',
  'code'
];

function repeatCounter (arrayinsert) {

arrayinsert.forEach(function (x) { arrayinsert[x] = (arrayinsert[x] || 0) + 1; });
console.log(arrayinsert)
}

repeatCounter(counterWords);