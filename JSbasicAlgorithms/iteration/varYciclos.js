

// iteration 1 --VARIABLES--

{
    let myFavoriteHero = 'Hulk'

    let x = 50;

    let h = 5;

    let y = 10;

    let z = y + h;
}




// iteration 2 --VARIABLES AVANZADAS--

// #1:  cambia el valor de la propiedad age a 25.

const character = { name: 'Jack Sparrow', age: 10 };

character.age = '25';



// #2: Declara 3 variables con los nombres y valores siguientes

firstName = 'Jon';
lastName = 'Snow';
age = 24;

// Muestralos por consola de esta forma: 

console.log('Soy ' + firstName + ' ' + lastName + ' ' + 'tengo ' + age + ' ' + 'y me gustan los lobos')


// #3: Dado el siguiente javascript, imprime con un console.log la suma del precio de ambos juguetes.

const toy1 = { name: 'Buss myYear', price: 19 };
const toy2 = { name: 'Rallo mcKing', price: 29 };

console.log('precio juguete iteration 3 es =', toySuma = toy1.price + toy2.price)


// #4: Dado el siguiente javascript, actualiza el valor de la variable globalBasePrice a 25000 
// y actualiza la propiedad finalPrice de todos los coches con el valor de su propiedad 
// basePrice más el valor de la variable globalBasePrice.

let globalBasePrice = 10000;
const car1 = { name: 'BMW m&m', basePrice: 50000, finalPrice: 60000 };
const car2 = { name: 'Chevrolet Corbina', basePrice: 70000, finalPrice: 80000 };

let globalBasePriceAct = 25000;
let car1Act = { name: 'BMW m&m', basePrice: 50000, finalPriceAct: car1.basePrice + globalBasePriceAct };
let car2Act = { name: 'Chevrolet Corbina', basePrice: 70000, finalPriceAct2: car2.basePrice + globalBasePriceAct };
console.log('precio final actualizado de coches es BMW ', car1Act.finalPriceAct, 'Y CHEVROLET', car2Act.finalPriceAct2)




//iteration 3


// 1.1 Multiplica 10 por 5 y muestra el resultado mediante alert.

alert('10' + ' x' + '5' + ' = ' + 10 * 5)

// 1.2 Divide 10 por 2 y muestra el resultado en un alert.

alert('10' + ' /' + '2' + ' = ' + 10 / 2)

// 1.3 Muestra mediante un alert el resto de dividir 15 por 9.

alert('resto de' + '15' + ' /' + '9 ' + ' = ' + 15 % 9)

// 1.4 Usa el correcto operador de asignación que resultará en x = 15, 
// teniendo dos variables y = 10 y z = 5.
{
    let y = 10;
    let z = 5;
    let x = y + z
    console.log(x)
}

// 1.5 Usa el correcto operador de asignación que resultará en x = 50,
// teniendo dos variables y = 10 y z = 5.

{
    let y = 10;
    let z = 5;
    let x = y * z
    console.log(x)
}



//iteration 4


//1.1 Consigue el valor "HULK" del array de cars y muestralo por consola.
{
    const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

    console.log('PRIMER VENGADOR' + ' ' + avengers[0])

}


//1.2 Cambia el primer elemento de avengers a "IRONMAN"

{

    const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
    { console.log(avengers) }


    avengers[0] = 'IRONMAN'

    console.log(avengers)

}

//1.3 Alert numero de elementos en el array usando la propiedad correcta de Array.
{
    const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
    alert('numero de vengadores activos' + ' ' + avengers.length)
}

//1.4 Añade 2 elementos al array: "Morty" y "Summer". 
//Muestra en consola el último personaje del array
{
    const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
    rickAndMortyCharacters.push('Morty', 'Summer');

    console.log(rickAndMortyCharacters)

    console.log(rickAndMortyCharacters[rickAndMortyCharacters.length - 1] + ' es el ultimo Pj agregado')

}

//1.5 Elimina el último elemento del array y muestra el primero y el último por consola.
{
    const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
    rickAndMortyCharacters.pop();
    console.log(rickAndMortyCharacters)

}
//1.6 Elimina el segundo elemento del array y muestra el array por consola.

{
    const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];


    let remove = rickAndMortyCharacters.splice(1, 1);

    console.log(rickAndMortyCharacters)

}


//iteration 5

const number1 = 10;
const number2 = 20;
const number3 = 2;

// ejemplo
if (number1 === 10) {
    console.log('number1 es estrictamente igual a 10')
}

if ( ( (number2 / number1) == 2) ) {
    console.log("number2 dividido entre number1 es igual a 2");
}

if (number1 !== number2) {
    console.log("number1 es estrictamente distinto a number2");
}

if (number3 != number1) {
    console.log("number3 es distinto number1");
}

if (
      ( (number3 * 5) ==   number1  ) 
     
     ) 
     {
    console.log("number3 por 5 es igual a number1");
}

if ( 
    ((number3 * 5) == number1) & ((number1 * 2) == number2)
) {
    console.log("number3 por 5 es igual a number1 Y number1 por 2 es igual a number2");
}

if ((((number2 / 2) == number1) | ((number1 / 5) == number3))) {
    console.log("number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3");
}



// iteration 6


//1.1 Crea un bucle for que vaya desde 0 a 9 y muestralo por consola.

for (i=0; i<=9; i++ ) {
    console.log (i);    
} 


/* 1.2 Crea un bucle for que vaya desde 0 a 9 y muestralo por consola solo 
cuando el resto del numero dividido entre 2 sea 0. */

for (i=0; i<=9; i++) {
    if  (i%2 === 0)
    { console.log (i) }

}

/* 1.3 Crea un bucle para conseguir dormir contando ovejas. 
Este bucle empieza en 0 y termina en 10. 
Muestra por consola un mensaje diciendo 'Intentando dormir' en cada vuelta del bucle 
y cambia el mensaje en la última vuelta a 'Dormido!'. */

for (i=0; i<=10; i++)
{
    if (i<10) {
    console.log ('intentando dormir' , i ) }
    else 
    {  console.log('dormido', i )   }
}