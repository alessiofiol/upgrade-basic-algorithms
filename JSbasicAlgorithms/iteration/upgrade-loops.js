/* Iteración #1: Usa includes
Haz un bucle y muestra por consola todos aquellos valores del array 
que incluyan la palabra "Camiseta". Usa la función .includes de javascript. */


const products = [
    'Camiseta de Pokemon',
    'Pantalón coquinero',
    'Gorra de gansta',
    'Camiseta de Basket',
    'Cinrurón de Orión',
    'AC/DC Camiseta'
]
for (const iterator of products) { if (iterator.includes('Camiseta')) {console.log(iterator)}}


    
/* Iteración #2: Condicionales avanzados
Comprueba en cada uno de los usuarios que tenga al menos dos 
trimestres aprobados y añade la propiedad isApproved a true 
o false en consecuencia. Una vez lo tengas compruébalo con un 
console.log.  */

const alumns = [
        {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, // reprobado    
		{name: 'Lucia Aranda', T1: true, T2: false, T3: true}, // aprob
		{name: 'Juan Miranda', T1: false, T2: true, T3: true}, // aprob
		{name: 'Alfredo Blanco', T1: false, T2: false, T3: false}, // reprob
		{name: 'Raquel Benito', T1: true, T2: true, T3: true} // aprob
]



/* for (const iterator of alumns) {
    let isApproved = true
    let contador = 0
    if (iterator.T1 === false) {
        contador =+ 1
        
    }
    else if (iterator.T2 === false) {
        contador =+ 1
    } 
    else if (iterator.T3 === false) {
        contador =+ 1
    }
    if (contador > 1 ) {
        isApproved=false
  
    }
    console.log(`${iterator.name} aprobado ?: ${isApproved}`)   
}

 */


console.log(`



`)
/* 
Iteración #3: Probando For...of

Usa un bucle forof para recorrer todos los destinos del array. 
Imprime en un console.log sus valores.

 */

const placesToTravel = ['Japon', 'Venecia', 'Murcia', 'Santander', 'Filipinas', 'Madagascar']

for (const iterator of placesToTravel) {
    console.log(iterator    )
}

console.log(`



`)




/* 
Iteración #4: Probando For...in

Usa un for...in para imprimir por consola los datos del alienígena.. Puedes usar este objeto:
 */

const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg'
}


for (const datos in alien) {
    if (Object.hasOwnProperty.call(alien, datos)) {
        const element = alien[datos];
        console.log(element)
    }
}


console.log(`



`)


